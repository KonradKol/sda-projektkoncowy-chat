package sda.controller;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;

@Service
public class MessageRemoteService extends AbstractRemoteService {
	public List<Message> getNewMessages() {
		HttpHeaders headers = getDefaultHeaders();
		RequestEntity request = new RequestEntity(headers, HttpMethod.GET, prepareUrl("/api/message"));
		return restTemplate.exchange(request, new ParameterizedTypeReference<List<Message>>() {
		}).getBody();
	}

	public Message createMessage(Message message) {
		HttpHeaders headers = getDefaultHeaders();
		RequestEntity request = new RequestEntity(message, headers, HttpMethod.POST, prepareUrl("/api/message"));
		return restTemplate.exchange(request, Message.class).getBody();
	}
}