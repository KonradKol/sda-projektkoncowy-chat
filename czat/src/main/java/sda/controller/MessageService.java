package sda.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import user.UserService;

@Service
public class MessageService {

	@Autowired
	MessageRepository messageRepository;
	
	
	
	@Transactional
	public void create(Message message){
		
		messageRepository.create(message);
	}
	
	@Transactional
	public  List<Message>getAllMessages(){
	return	messageRepository.getAllMessages();		
	}
	
	
}
