package sda.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/message")
public class MessageController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private MessageService messageService;
	
	@Autowired
	private MessageRemoteService   messageRemoteService;

	
	
	@PostMapping("/")
	public Message createMessage(@RequestBody Message message) {
		message.setCreated(new Date());
		Message remoteMessage=messageRemoteService.createMessage(message);
		message.setId(remoteMessage.getId());
		messageService.create(message);
		return message;
	}

	@GetMapping("/")
	public List<Message> getAllMessages() {
		return messageService.getAllMessages();
	}
	
	@GetMapping("/new")
	public List<Message> getNewMessages() {
		return messageRemoteService.getNewMessages();
	}

}
