package sda.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class MessageRepository {

	@PersistenceContext
	private EntityManager em;
	
	public void create(Message message){
em.persist(message);		
	}
	public List<Message> getAllMessages(){
		return em.createNativeQuery("SELECT * FROM message", Message.class)
				.getResultList();
	}

	
}
