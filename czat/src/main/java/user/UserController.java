package user;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sda.controller.MessageService;

@RestController
@RequestMapping("/message")
public class UserController {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private MessageService messageService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRemoteService userRemoteService;
	
	@PostMapping("/user")
	public User createUser(@RequestBody User user) {
	userService.createUser(user);
	
		return user;
	}
	@GetMapping("/users")
	public  List<String> getUsers(){
		return userRemoteService.getAvailableContacts();
	}


}
