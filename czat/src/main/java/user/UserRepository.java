package user;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.catalina.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {
	@PersistenceContext
	private EntityManager em;

	public User getUserByName(String name) {
		try {
			return (User) em.createNativeQuery("SELECT * FROM USERS where USER_NAME=?", User.class)
					.setParameter(1, name).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public void createUser(String user) {
		em.persist(user);
	}
}
