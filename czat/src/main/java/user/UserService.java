package user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sda.controller.MessageService;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	MessageService messageService;

	@Transactional
	public void createUser(User user) {
		if (userRepository.getUserByName(user.getUserName()) == null) {
			userRepository.createUser(user.getUserName());
		}
	}
}
