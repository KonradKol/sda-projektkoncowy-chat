 function displayAllMessages(){
    $.get("/message/", function(data){
        for(var i=0;i<data.length;i++){
            appendMessage(data[i]);
        }
    });
     
}

$(function(){
    $.ajaxSetup({contentType: 'application/json'});  
    $("#send").click(function(){
        $.post("message/", JSON.stringify({
            senderId : $("#senderId").val(),
            receiverId : $("#receiverId").val(),
            body : $("#body").val()
        }));
    });
    $("#refresh").click(function(){
        $.get("message/", function(data){
            for(var i=0;i<data.length;i++){
                appendMessage(data[i]);
            }
        });
    });
    displayAllMessages();
});

function appendMessage(message){   // ładne wyświetlanie
        $('.list-group').append('<li class="list-group-item list-group-item-info">'
            + '[' + message.senderId + '] '
            + message.body
            + '<div style="float:right">' + formatDate(message.created)
            + '</div></li>');
};

function formatDate(epochDate){
    return new Date(epochDate);
};